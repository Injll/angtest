(function(){
	var app = angular.module("ComplexForm",[]);
	
	app.controller("FormController",["$scope","$log",function($scope,$log){
		$log.log($scope);
		this.fieldsets = angular.element(userInfo).children()
		this.checkSets = function(){
			for (var i = 0; i < this.fieldsets.length; i++ ){
				$log.log(this.fieldsets[i]);
				angular.element(this.fieldsets[i]).addClass('ng-invalid ng-inactive');
				var validity = this.validateSet(this.fieldsets[i]);
				$log.log(validity);
			}
		};
		this.validateSet = function(fieldset){
			var groups = angular.element(fieldset).find('.form-group');
			$log.log(groups);
			for(var i = 0; i < groups.length; i++ ){
				var group = groups[i];
				var inputs = angular.elements(group).find('inputs');
				if(inputs.length){
					var parent = angular.element(inputs[0]).parent();
					
					if( 
						angular.element(parent).hasClass('form-group') && 
						inputs.length === 1 && 
						angular.element(inputs[0]).hasClass('ng-valid')
					){
						angular.element(group).addClass('ng-valid').removeClass('ng-invalid');
						return true;
					}
					else{
						var isRadio = angular.element(parent).parent().hasClass("radio") ;
						var isCheckbox = angular.element(parent).parent().hasClass("radio");
						if( (isRadio||isCheckbox) && angular.element(inputs[0]).hasClass('ng-valid')){
							angular.element(group).addClass('ng-valid').removeClass('ng-invalid');
							return true;
						}
						else{
							return false;
						}
					}
					
					
				}
				
			}
			
		}
		this.checkSets();
		/*
		gather fieldsets of form (children), 
		deduct form variables, 
		test for fieldset validity
		disable invalid forms if not first invalid form*/
	}]);
	//console.log(app);
})();